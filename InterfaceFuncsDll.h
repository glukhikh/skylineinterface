#pragma once

#ifdef INTERFACEFUNCSDLL_EXPORTS
#define INTERFACEFUNCSDLL_API __declspec(dllexport) 
#else
#define INTERFACEFUNCSDLL_API __declspec(dllimport) 
#endif

namespace InterfaceFuncs
{
	// ���� ����� �������������� �� InterfaceFuncsDll.dll
	class MyInterfaceFuncs
	{
	public:
		// ������������� ������ �� ����������� � ������� ��� "�������"
		static INTERFACEFUNCSDLL_API void PutCursor(int X_COORD, int Y_COORD);

		// ������������� ������ �� �����������, ������� ��� "�������" � �������� ���� ������ � ��� �� ��� 
		//(�����! ���� ���� �������� ������ �� ��������� �������� ������)
		static INTERFACEFUNCSDLL_API void ColorTextAndBack(int X_coord, int Y_coord, int Text_Color, int Background_Color, char*str);

		// ��������� ������ ��������� �� �����
		static INTERFACEFUNCSDLL_API void RecFileToArray(char **array, int arrayX, int arrayY, const char*filename);

	};
}