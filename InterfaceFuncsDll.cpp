// InterfaceFuncsDll.cpp: ���������� ���������������� ������� ��� ���������� DLL.
//

#include "stdafx.h"
#include "InterfaceFuncsDll.h"
#include <iostream>
#include <fstream>

using namespace std;

namespace InterfaceFuncs
{
	// ������������� ������ �� ����������� � ������� ��� "�������"
	void MyInterfaceFuncs::PutCursor(int X_coord, int Y_coord)
	{
		HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE); // ����� ������ ��� ������ ������� � �������
		CONSOLE_CURSOR_INFO ci;
		ci.bVisible = 0;
		ci.dwSize = 100;
		SetConsoleCursorInfo(hConsole, &ci); // ������ �������� ������

		COORD c;
		c.X = X_coord; c.Y = Y_coord;
		SetConsoleCursorPosition(hConsole, c); //��������� �������
	}
	
	// ������������� ������ �� ����������� � �������� ���� ������ � ��� �� ��� 
	//(�����! ���� ���� �������� ������ �� ��������� �������� ������)
	void MyInterfaceFuncs::ColorTextAndBack(int X_coord, int Y_coord, int Text_Color, int Background_Color, char*str)
	{
		MyInterfaceFuncs::PutCursor(X_coord, Y_coord);
		HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE); // ����� ������ ��� ������ ������� � �������

		SetConsoleTextAttribute(hConsole, (WORD)((Background_Color << 4) | Text_Color)); //��������� ����� ������
		cout << str << endl;
	}

	// ��������� ������ ��������� �� �����
	void MyInterfaceFuncs::RecFileToArray(char **array, int arrayX, int arrayY, const char*filename)
	{
		char* str = new char[arrayX + 1];
		ifstream txt(filename); // �������� �����
		for (int y = 0; y < arrayY; y++) // ���� ������������ ������ ������ �� �����
		{
			txt.getline(str, sizeof(char)*(arrayX + 1)); // ��������� ������ �� �����
			for (int x = 0; x <= arrayX; x++) // ������ ������� �� ������������ ������� �����
				array[y][x] = str[x];
		}
		txt.close(); // �������� �����
	}
}